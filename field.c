/*
 * XDots. Copyright (C) 2020 Eugene P. <pieu@mail.ru>
 *
 * This file is part of XDots.
 *
 * XDots is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * XDots is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XDots.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "xdots.h"
#include "field.h"

#define O -1 // Mark outside space
#define C -2 // Mark connected dot
#define I -3 // Mark inside space

const struct dot player1 = {P1N, P1C, P1D, P1Z};
const struct dot player2 = {P2N, P2C, P2D, P2Z};

// Field size [FIELD_WIDTH + 2] x [FIELD_HEIGHT + 2]

int  dots[FIELD_WIDTH + 2][FIELD_HEIGHT + 2];
int walls[FIELD_WIDTH + 2][FIELD_HEIGHT + 2];

static int m[FIELD_WIDTH + 2][FIELD_HEIGHT + 2]; // Marks array

#ifdef DBG_PRINT_DOTS
//--------------------------------------------------------------/ print_dots /--
static void print_dots()
{
	for (register int y = 0; y < FIELD_HEIGHT + 2; y++) {
		for (register int x = 0; x < FIELD_WIDTH + 2; x++)
			printf("%X", dots[x][y]);
		printf("\n");
	}
	printf("\n");
}
#endif

#ifdef DBG_PRINT_MARKS
//-------------------------------------------------------------/ print_marks /--
static void print_marks()
{
	for (register int y = 0; y < FIELD_HEIGHT + 2; y++) {
		for (register int x = 0; x < FIELD_WIDTH + 2; x++) {
			switch (m[x][y]) {
			case O:
				printf(".");
				break;
			case I:
				printf(" ");
				break;
			case C:
				printf("@");
				break;
			default:
				printf("%X", m[x][y]);
			}
		}
		printf("\n");
	}
	printf("\n");
}
#endif

//---------------------------------------------------------------- field_init --
int field_init()
{
	register int x, y;

	for (y = 1; y <= FIELD_HEIGHT; y++)
		for (int x = 1; x <= FIELD_WIDTH; x++) {
			dots[x][y] = 0;
			walls[x][y] = 0;
		}

	for (y = 0; y < FIELD_HEIGHT + 2; y++)
		m[0][y] = m[FIELD_WIDTH + 1][y] = O;

	for (x = 1; x < FIELD_WIDTH + 1; x++)
		m[x][0] = m[x][FIELD_HEIGHT + 1] = O;

	return 1;
}

//------------------------------------------------------------- field_add_dot --
int field_add_dot(const struct dot *p, int x, int y)
{
	if (dots[x][y] != Z && dots[x][y] != p->z)
		return 0;

	dots[x][y] = p->n;

	return 1;
}

/*---------------------------------------------------------------- mark_walls --
 *      / 0x1   [x-1][y-1]   [ x ][y-1]   [x+1][y-1]
 *     o- 0x2   [x-1][ y ]   [ x ][ y ]   [x+1][ y ]
 * 0x8 |\ 0x4   [x-1][y+1]   [ x ][y+1]   [x+1][y+1]
 */

#define EDGE(x, y) \
( \
	(m[x-1][y-1] != C && m[x-1][y-1] != I) || \
	(m[ x ][y-1] != C && m[ x ][y-1] != I) || \
	(m[x+1][y-1] != C && m[x+1][y-1] != I) || \
	(m[x+1][ y ] != C && m[x+1][ y ] != I) || \
	(m[x+1][y+1] != C && m[x+1][y+1] != I) || \
	(m[ x ][y+1] != C && m[ x ][y+1] != I) || \
	(m[x-1][y+1] != C && m[x-1][y+1] != I) || \
	(m[x-1][ y ] != C && m[x-1][ y ] != I) || \
	(m[x-1][y-1] != C && m[x-1][y-1] != I) \
)

static inline void mark_walls(const struct dot *p)
{

	register int x, y;

	for (y = 1; y <= FIELD_HEIGHT; y++)
		for (x = 1; x <= FIELD_WIDTH; x++) {

			if (m[x][y] != C || !EDGE(x, y))
				continue;

			if (m[x+1][y] == C) {
				walls[x][y] |= 0x2;
				dots[x][y] = dots[x+1][y] = p->c;
			}

			if (m[x][y+1] == C) {
				walls[x][y] |= 0x8;
				dots[x][y] = dots[x][y+1] = p->c;
			}

			if (m[x+1][y-1] == C && 
					m[x][y-1] != C && m[x+1][y] != C) {
				walls[x][y] |= 0x1;
				dots[x][y] = dots[x+1][y-1] = p->c;
			}

			if (m[x+1][y+1] == C && 
					m[x+1][y] != C && m[x][y+1] != C) {
				walls[x][y] |= 0x4;
				dots[x][y] = dots[x+1][y+1] = p->c;
			}
		}
}

//--------------------------------------------------------------- field_check --
int field_check(const struct dot *p1)
{
	register int x, y;
	char repeat;

	const struct dot *p2 = (p1 == &player1) ? &player2 : &player1;

	for (y = 1; y <= FIELD_HEIGHT; y++)
		for (x = 1; x <= FIELD_WIDTH; x++)
			m[x][y] = dots[x][y];
	/* 
	 * Mark outside
	 * ..........
	 * ..1...1...
	 * .121.101..
	 * .11...1...
	 * ..........
	 * 
	 * '.'=O, 1=P1N, 2=P2N, 0=Z (empty)
	 */ 

	do {
		repeat = 0;
		for (y = 1; y <= FIELD_HEIGHT; y++) {
			for (x = 1; x <= FIELD_WIDTH; x++) {
				if (m[x][y] == p1->n || m[x][y] == p1->c || 
				    m[x][y] == O)
					continue;

				if (m[x-1][y] == O || m[x+1][y] == O ||
				    m[x][y-1] == O || m[x][y+1] == O) {
					m[x][y] = O;
					repeat = 1;
				}
			}
		}
	} while (repeat);

	/*
	 * Mark captured dots
	 */

	int captured = 0;
	for (y = 1; y <= FIELD_HEIGHT; y++)
		for (x = 1; x <= FIELD_WIDTH; x++) {

			if (m[x][y] == p2->z)
				dots[x][y] = p1->z;

			else if (m[x][y] == p1->d)
				dots[x][y] = p1->n;

			else if (m[x][y] == p2->n || m[x][y] == p2->c) {
				dots[x][y] = p2->d;
				m[x][y] = I;
				captured++;
			}
		}

	if (!captured)
		return 0;

	/* 
	 * Mark inside
	 * ..........
	 * ..@...1...
	 * .@ @.101..
	 * .1@...1...
	 * ..........
	 * 
	 * '.'=O, ' '=I, '@'=C, 1=P1N, 0=Z (empty)
	 */ 
	do {
		repeat = 0;
		for (y = 1; y <= FIELD_HEIGHT; y++)
			for (x = 1; x <= FIELD_WIDTH; x++) {

				if ((m[x][y] != p1->n && m[x][y] != p1->c &&
				     m[x][y] != Z) || 
				    !(m[x][y-1] == I || m[x][y+1] == I ||
				    m[x-1][y] == I || m[x+1][y] == I))
					continue;

				repeat = 1;
				if (dots[x][y] == Z) {
					dots[x][y] = p1->z;
					m[x][y] = I;
				} else
					m[x][y] = C;
			}
	} while (repeat);

	mark_walls(p1);

#ifdef DBG_PRINT_DOTS
	print_dots();
#endif

#ifdef DBG_PRINT_MARKS
	print_marks();
#endif
	return captured;
}

//------------------------------------------------------ field_count_captured --
int field_count_captured(const struct dot *p)
{
	int score = 0;

	for (register int y = 1; y <= FIELD_HEIGHT; y++)
		for (register int x = 1; x <= FIELD_WIDTH; x++)
			if (dots[x][y] == p->d)
				score++;
	return score;
}
