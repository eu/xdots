struct dot {
	// normal, connected, dead, zero (closed empty space)
	int n, c, d, z;
};

extern const struct dot player1, player2;

int field_init(void); 
int field_add_dot(const struct dot *p, int x, int y);
int field_check(const struct dot *p1);
int field_count_captured(const struct dot *p1);
