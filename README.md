## Dots game
#### *A game of strategy, where you have to place your dots carefully to surround the opponent.*
##### Initial Xlib remake of an old Windows version of the game: 
##### https://codeberg.org/eu/misc/raw/branch/main/c/csdots/dots.exe

### Usage
```sh
make
./xdots
```
### License: GPLv3
