CC = cc
CFLAGS = -O3 -Wall -Wextra
LDFLAGS =
LIBS = -lX11

all: xdots

xdots: xdots.o field.o Xlib/app.o Xlib/draw.o
	$(CC) $(LDFLAGS) $(LIBS) $^ -o $@

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	@clear
	@rm -f Xlib/*.o *.o xdots *~
