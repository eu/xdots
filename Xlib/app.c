/*
 * XDots. Copyright (C) 2020 Eugene P. <pieu@mail.ru>
 *
 * This file is part of XDots.
 *
 * XDots is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XDots is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XDots.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>

#include "../xdots.h"
#include "../field.h"
#include "app.h"
#include "draw.h"

#define FW (1 + FIELD_WIDTH)
#define FH (1 + FIELD_HEIGHT)

#define EVENTS ExposureMask | ButtonPressMask | StructureNotifyMask

static struct x_app *X;

//------------------------------------------------------------------ app_exit --
void app_exit(char *str)
{
	char msg[sizeof(str) + 20];

	sprintf(msg, "%s error: %s\n", CAPTION, str);
	fprintf(stderr, msg);

	exit(1);
}

//------------------------------------------------------------------ app_init --
void app_init()
{
	X = (struct x_app *) malloc(sizeof(struct x_app));
	X->xs_grid = (XSegment *) malloc((FW + FH + 2) * sizeof(XSegment));
	X->xs = (XSegment *) malloc(FW * FH * sizeof(XSegment));
	X->xa = (XArc *) malloc(FW * FH * sizeof(XArc));
	if (!X || !X->xs || !X->xa)
		app_exit("Out of memory");

	X->display = XOpenDisplay(NULL);
	if (!X->display)
		app_exit("!XOpenDisplay()");

	X->screen = DefaultScreen(X->display);
	X->gc = DefaultGC(X->display, X->screen);
	X->window_width = FW * GRID_SIZE;
	X->window_height = FH * GRID_SIZE;
	X->grid_size = GRID_SIZE;
	X->dot_size = GRID_SIZE / 3 + DOT_SIZE;
	X->x_res = DisplayWidth(X->display, X->screen);
	X->y_res = DisplayHeight(X->display, X->screen);
	X->window = XCreateSimpleWindow(X->display,
					RootWindow(X->display, X->screen),
					(X->x_res - X->window_width) / 2,
					(X->y_res - X->window_height) / 2,
					X->window_width, X->window_height, 1,
					BlackPixel(X->display, X->screen),
					WhitePixel(X->display, X->screen));

	XSizeHints *sh = XAllocSizeHints();
	if (!sh)
		app_exit("!XAllocSizeHints()");

	sh->flags = PAspect;
	sh->min_aspect.x = sh->max_aspect.x = FW;
	sh->min_aspect.y = sh->max_aspect.y = FH;
	XSetWMNormalHints(X->display, X->window, sh);
	XStoreName(X->display, X->window, CAPTION);

	Atom delWindow = XInternAtom(X->display, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(X->display, X->window, &delWindow, 1);
	XSelectInput(X->display, X->window, EVENTS);
	XMapWindow(X->display, X->window);

	X->colormap = DefaultColormap(X->display, X->screen);

	if (!XAllocNamedColor(X->display, X->colormap, BACKGROUND_COLOR,
			      &X->color.background, &X->color.background) ||
	    !XAllocNamedColor(X->display, X->colormap, GRID_COLOR,
			      &X->color.grid, &X->color.grid) ||
	    !XAllocNamedColor(X->display, X->colormap, PLAYER1_COLOR,
			      &X->color.p1, &X->color.p1) ||
	    !XAllocNamedColor(X->display, X->colormap, PLAYER2_COLOR,
			      &X->color.p2, &X->color.p2))
		app_exit("!XAllocNamedColor()");

	XSetWindowBackground(X->display, X->window, X->color.background.pixel);
	XClearWindow(X->display, X->window);

	field_init();
	draw_init(X);
	draw_grid(X);
}

//--------------------------------------------------- app_event_resize_window --
static void app_event_resize_window(int width, int height)
{
	X->window_width = width;
	X->window_height = height;
	X->grid_size = MIN(X->window_width / FW, X->window_height / FH);
	X->dot_size = X->grid_size / 3 + DOT_SIZE;

	if (width != X->x_res && height != X->y_res)
		XResizeWindow(X->display, X->window, FW * X->grid_size + 1,
			      FH * X->grid_size + 1);
	draw_init(X);
}

//---------------------------------------------------------- app_redraw_field --
static void app_redraw_field()
{
	draw_grid(X);
	draw_dots(X, &player1);
	draw_dots(X, &player2);
	draw_walls(X, &player1);
	draw_walls(X, &player2);
}
//-------------------------------------------------------------- app_dispatch --
void app_dispatch()
{
	XEvent e;
	int w, h;

	while (1) {
		XNextEvent(X->display, &e);

		switch (e.type) {

		// Redraw window
		case Expose:
			app_redraw_field();
			break;

		// Window state
		case ConfigureNotify:
			w = e.xconfigure.width;
			h = e.xconfigure.height;
			if (w == X->window_width && h == X->window_height)
				break;
			app_event_resize_window(w, h);
			break;

		// Mouse click
		case ButtonPress:
#ifdef DBG_ADD_DOT
			if (e.xbutton.button == Button3)
				draw_move(X,&player2, e.xbutton.x, e.xbutton.y);
#endif
			if (e.xbutton.button != Button1)
				break;
			if (draw_move(X, &player1, e.xbutton.x, e.xbutton.y)) {
				// ai();
			}
			break;

		// Close window
		case ClientMessage:
			return;
		}
	}
}

//----------------------------------------------------------------- app_close --
void app_close()
{
	XFreeColormap(X->display, X->colormap);
	XDestroyWindow(X->display, X->window);
	XCloseDisplay(X->display);

	free(X->xa);
	free(X->xs);
	free(X->xs_grid);
	free(X);
}
