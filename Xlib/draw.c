/*
 * XDots. Copyright (C) 2020 Eugene P. <pieu@mail.ru>
 *
 * This file is part of XDots.
 *
 * XDots is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * XDots is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XDots.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <X11/Xlib.h>
#include <stdio.h>

#include "app.h"
#include "../xdots.h"
#include "../field.h"

//----------------------------------------------------------------- draw_init --
void draw_init(struct x_app *X)
{
	register int i, n = 0;

	int field_width  = (FIELD_WIDTH  + 1) * X->grid_size;
	int field_height = (FIELD_HEIGHT + 1) * X->grid_size;

	for (i = 0; i <= field_width; i += X->grid_size) {
		X->xs_grid[n].x1 = i;
		X->xs_grid[n].y1 = 0;
		X->xs_grid[n].x2 = i;
		X->xs_grid[n++].y2 = field_height;
	}

	for (i = 0; i <= field_height; i += X->grid_size) {
		X->xs_grid[n].x1 = 0;
		X->xs_grid[n].y1 = i;
		X->xs_grid[n].x2 = field_width;
		X->xs_grid[n++].y2 = i;
	}

	X->xs_grid_size = n;
}

//----------------------------------------------------------------- draw_grid --
void draw_grid(struct x_app *X)
{
	int field_width  = (FIELD_WIDTH + 1) * X->grid_size;
	int field_height = (FIELD_WIDTH + 1) * X->grid_size;

	XSetForeground(X->display, X->gc, X->color.grid.pixel);

	XSetLineAttributes(X->display, X->gc, 1, LineSolid, CapRound,
			   JoinRound);

	XDrawSegments(X->display, X->window, X->gc, X->xs_grid,
		      X->xs_grid_size);

	XDrawRectangle(X->display, X->window, X->gc, 2, 2, field_width - 4, 
		       field_height - 4);
}

//----------------------------------------------------------------- draw_dots --
void draw_dots(struct x_app *X, const struct dot *p)
{
	register int y, x, window_x, window_y;
	unsigned long color;

	color = (p == &player1) ? X->color.p1.pixel : X->color.p2.pixel;

	XSetForeground(X->display, X->gc, color);
	for (x = 1; x <= FIELD_WIDTH; x++) {
		for (y = 1; y <= FIELD_HEIGHT; y++) {

			if (dots[x][y] != p->n && dots[x][y] != p->c && 
			    dots[x][y] != p->d)
				continue;

			window_x = x * X->grid_size - X->dot_size / 2;
			window_y = y * X->grid_size - X->dot_size / 2;
			XFillArc(X->display, X->window, X->gc, window_x, 
				 window_y, X->dot_size, X->dot_size, 0, 360*64);
		}
	}
}

//---------------------------------------------------------------- draw_walls --
void draw_walls(struct x_app *X, const struct dot *p)
{
	register int x, y, x1, y1;
	int gs = X->grid_size, n = 0;
	unsigned long color;

	color = (p == &player1) ? X->color.p1.pixel : X->color.p2.pixel;

	XSetForeground(X->display, X->gc, color);
	XSetLineAttributes(X->display, X->gc, MAX(1, X->dot_size / 4), 
			   LineSolid, CapRound, JoinRound);

	for (y = 1; y <= FIELD_HEIGHT; y++)
		for (x = 1; x <= FIELD_WIDTH; x++) {

			if (dots[x][y] != p->c && dots[x][y] != p->d)
				continue;

			x1 = x * gs;
			y1 = y * gs;

			if (walls[x][y] & 0x1) {
				X->xs[n].x1 = x1;
				X->xs[n].y1 = y1;
				X->xs[n].x2 = x1 + gs;
				X->xs[n++].y2 = y1 - gs;
			}
			if (walls[x][y] & 0x2) {				
				X->xs[n].x1 = x1;
				X->xs[n].y1 = y1;
				X->xs[n].x2 = x1 + gs;
				X->xs[n++].y2 = y1;
			}
			if (walls[x][y] & 0x4) {
				X->xs[n].x1 = x1;
				X->xs[n].y1 = y1;
				X->xs[n].x2 = x1 + gs;
				X->xs[n++].y2 = y1 + gs;
			}
			if (walls[x][y] & 0x8) {
				X->xs[n].x1 = x1;
				X->xs[n].y1 = y1;
				X->xs[n].x2 = x1;
				X->xs[n++].y2 = y1 + gs;
			}
		}
	if (n)
		XDrawSegments(X->display, X->window, X->gc, X->xs, n);
}

//---------------------------------------------------------------- draw_score --
void draw_score(struct x_app *X)
{
	char caption[100];
	int p1_score, p2_score;

	p1_score = field_count_captured(&player2);
	p2_score = field_count_captured(&player1);

	sprintf(caption, "%s [ %d : %d ]", CAPTION, p1_score, p2_score);
	XStoreName(X->display, X->window, caption);	
}

//----------------------------------------------------------------- draw_move --
int draw_move(struct x_app *X, const struct dot *p1, int window_x, int window_y)
{
	int field_x, field_y;
	const struct dot *p2 = (p1 == &player1) ? &player2 : &player1;

	field_x = (int) (window_x / X->grid_size);
	if (window_x % X->grid_size > X->grid_size / 2)
		field_x++;

	field_y = (int) (window_y / X->grid_size);
	if (window_y % X->grid_size > X->grid_size / 2)
		field_y++;

	if (!field_x || field_x > FIELD_WIDTH || 
	    !field_y || field_y > FIELD_HEIGHT)
		return 0;

	if (!field_add_dot(p1, field_x, field_y))
		return 0;

	draw_dots(X, p1);

	if (field_check(p1)) {
		draw_walls(X, p1);
		draw_score(X);
	}
	if (field_check(p2)) {
		draw_walls(X, p2);
		draw_score(X);
	}
	return 1;
}
