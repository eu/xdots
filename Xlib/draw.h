#include "../xdots.h"
#include "../debug.h"

void draw_init(struct x_app *X);
void draw_grid(struct x_app *X);
void draw_dots(struct x_app *X, const struct dot *p);
void draw_walls(struct x_app *X, const struct dot *p);
void draw_score(struct x_app *X);
int draw_move(struct x_app *X, const struct dot *p, int window_x, int window_y);
