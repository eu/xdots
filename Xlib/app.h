#include <X11/Xlib.h>

struct x_app {
	Display *display;
	int screen;
	Window window;
	GC gc;
	Colormap colormap;

	struct xdots_colors {
		XColor background, grid, p1, p2;
	} color;

	XSegment *xs_grid;
	XSegment *xs;
	XArc *xa;

	int x_res, y_res;
	int window_width, window_height;
	int xs_grid_size, grid_size, dot_size;
};

void app_init();
void app_dispatch();
void app_close();
