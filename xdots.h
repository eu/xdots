#include "debug.h"

#define CAPTION "XDots"

#define FIELD_WIDTH		30
#define FIELD_HEIGHT		20
#define GRID_SIZE		32
#define DOT_SIZE		3

// Define colors
#define BACKGROUND_COLOR	"#BEBEBE"
#define GRID_COLOR		"#808080"
#define PLAYER1_COLOR		"#FF0000"
#define PLAYER2_COLOR		"#0000FF"

#define Z	0 // Empty space
#define P1N	1 // Normal dot
#define P2N	2 
#define P1C	3 // Connected dot
#define P2C	4
#define P1D	5 // Dead dot
#define P2D	6 
#define P1Z	7 // Captured empty space
#define P2Z	8

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

extern int  dots[FIELD_WIDTH + 2][FIELD_HEIGHT + 2];
extern int walls[FIELD_WIDTH + 2][FIELD_HEIGHT + 2];
